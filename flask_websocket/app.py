import os
import eventlet
eventlet.monkey_patch()
from flask import Flask
from flask_socketio import SocketIO
from routes import main_blueprint


def create_app(register_blueprint=True):
    app = Flask(__name__)
    app.debug = True
    app.secret_key = os.urandom(42)
    if register_blueprint:
        app.register_blueprint(main_blueprint)

    sock = SocketIO(app, message_queue='redis://localhost:6379/', async_mode='eventlet')
    return sock, app


socketio, application = create_app()


if __name__ == '__main__':
    socketio.run(application)
