/**
 * Create the plotly plot object on the webpage
 */
function makePlotly( x, y ){
    console.log('make plot')
    var plotDiv = document.getElementById("plot");
    var traces = [{
        x: x,
        y: y
    }];
    var layout = {
        font: {size: 14},
        margin: {t: 0},
        xaxis: {
            range: [x[0],  x[x.length - 1]],
            rangeslider: {range: [x[0], x[x.length - 1]]},
            type: 'linear'
        },
        yaxis: {
            range: [-10, 10]
        }
    };

    var additional_params = {
        responsive: true
    };

    Plotly.plot(plotDiv, traces, layout, additional_params);
};

var plot_start = 0;

/**
 * Update the plot
 */
function streamPlotly( x, y ){
    console.log('refresh');
    var plotDiv = document.getElementById("plot");
    var data_update = {x: [x], y: [y]};

    Plotly.update(plotDiv, data_update)
};

var url = 'http://' + document.domain + ':' + location.port
var socket = io.connect(url);

/**
 * Describes the behaviour when receiving `connect`, `init` and `update` via websocket
 */
socket.on('connect', function(msg) {
    console.log('connected to websocket on ' + url);
});

socket.on('init', function (msg) {
    plot_start = msg.x[0];
    makePlotly( msg.x, msg.y )
});

socket.on('update', function (msg) {
    streamPlotly( msg.x, msg.y )
});
