import time
from signal_generator import SensorSignalForLocalVis, SensorSignalForFlask, SensorSignalForWebsocketIO

if __name__ == "__main__":
    sensors = (
               SensorSignalForFlask("s1", 25, [-8, 3]),
               # SensorSignalForLocalVis("s2", 20, [-3, 3]),
               SensorSignalForWebsocketIO("s3", 100, [-1, 1]),
               SensorSignalForWebsocketIO("s4", 100, [-4, 1]),
               )
    try:
        for sensor in sensors:
            sensor.start_measuring()
        time.sleep(10)
        for sensor in sensors:
            sensor.stop_measuring()
    except KeyboardInterrupt:
        for sensor in sensors:
            sensor.stop_measuring()
