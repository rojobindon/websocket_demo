# Simple async websocket - Receives messages send by websocket_client
import asyncio
import websockets


counter = 0


async def recv(websocket, path):
    global counter
    async for message in websocket:
        counter += 1
        print(counter, message)


if __name__ == "__main__":
    start_server = websockets.serve(recv, "localhost", 8765)
    asyncio.get_event_loop().run_until_complete(start_server)
    try:
        print("websocket opened")
        asyncio.get_event_loop().run_forever()
    except KeyboardInterrupt:
        print("Exiting")
