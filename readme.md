# Websocket demo

This is a very naive implementation of how to: 

- send data from a client to a websockets server. 
- send data via websocket to a flask server and visualise them in a web browser.

The goal here is not performance, but just to have a first idea of how to use websocket
protocol. After a skimming through protocols, `MQTT` looks much more efficient than `websocket` for transferring data
over the wire.

## Instruction 

- The Flask server requires you to run `redis-server` beforehand. 
- Then, start the Flask server located in `flask_webocket`. Use `python app.py` .
- Start another websocket server with  `python websockets_sandbox_server.py` .
- Finally, start the sensors simulator `python run.py` .
