import time
import json
import websockets
import asyncio
import multiprocessing as mp
import numpy as np
import matplotlib.pyplot as plt
from flask_socketio import SocketIO

# Global variable - Used for debugging purposes and checking messages are received
counter = 0


class SensorSignal:
    """
    Base class for SensorSignal. A sensor generates data in a separate process.
    Define a child class with a custom acquire method.
    """
    def __init__(self, name, sample_rate, amplitude_range):
        """
        Parameters
        ----------
        name : str
            Sensor name
        sample_rate : int
            Rate (Hz) the data are send to different sockets
        amplitude_range: list
            Random noise min and max amplitudes
        """
        self.name = name
        self.sample_rate = sample_rate
        self.idle_time = 1.0 / sample_rate
        self.amp_max = max(amplitude_range)
        self.amp_min = min(amplitude_range)
        self.t = np.arange(0, 0.25, 0.001)
        self.clean_signal = np.sin(2.0 * np.pi * 50 * self.t) + np.sin(2.0 * np.pi * 120 * self.t)
        self.signal = None
        self.worker = None
    
    def generate_signal(self):
        """ Add rdm noise to clean signal """
        self.signal = self.clean_signal + (np.random.rand(self.t.size) * (self.amp_max - self.amp_min) + self.amp_min)
    
    def start_measuring(self):
        self.worker = mp.Process(target=self.acquire, daemon=True)
        self.worker.start()
        print(f"{self.name} started")
    
    def stop_measuring(self):
        self.worker.terminate()
        self.worker.join()
        print(f"{self.name} stopped")
    
    def acquire(self):
        """ Overload in child class """
        raise NotImplementedError


class SensorSignalForFlask(SensorSignal):
    """ Generate data + uses flask socketIO.emit to communicate with flask server via websocket """
    def acquire(self):
        global counter
        socketio = SocketIO(message_queue='redis://localhost:6379/')
        try:
            self.generate_signal()
            y = self.signal.tolist()
            t = self.t.tolist()
            socketio.emit('init', {'x': t, 'y': y})
            while True:
                self.generate_signal()
                y = self.signal.tolist()
                counter += 1
                socketio.emit('update', {'x': t, 'y': y})
                
                print(f"{self.name} sending {counter}")
                time.sleep(self.idle_time)
        except KeyboardInterrupt:
            print(0)


class SensorSignalForLocalVis(SensorSignal):
    """ Generate data + live display with matplotlib """
    def acquire(self):
        """ Sandbox function - Generate data and display them live with matplotlib """
        try:
            while True:
                self.generate_signal()
                plt.plot(self.t, self.signal)
                plt.draw()
                plt.pause(self.idle_time)
                plt.clf()
        except KeyboardInterrupt:
            print(0)


class SensorSignalForWebsocketIO(SensorSignal):
    """ Generate data + uses websockets package - websockets_sandbox_server.py is the server """
    @staticmethod
    async def send_data(data):
        uri = "ws://localhost:8765"
        data = json.dumps(data[0:2])
        async with websockets.connect(uri) as websocket:
            await websocket.send(data)

    def acquire(self):
        """ Generate data + uses flask socketIO.emit to communicate with websockets server """
        global counter
        try:
            self.generate_signal()
            while True:
                self.generate_signal()
                data = self.prepare_data()
                asyncio.get_event_loop().run_until_complete(self.send_data(data))
                print(f"{self.name} sending {counter}")
                time.sleep(self.idle_time)
        except KeyboardInterrupt:
            print(0)

    def prepare_data(self):
        global counter
        counter += 1
        data = [self.name, counter, self.signal.tolist()]
        return data


if __name__ == "__main__":
    sensors = (
               SensorSignalForFlask("s1", 25, [-8, 3]),
               SensorSignalForLocalVis("s2", 20, [-3, 3]),
               SensorSignalForWebsocketIO("s3", 100, [-1, 1]),
               SensorSignalForWebsocketIO("s4", 100, [-4, 1]),
               )
    try:
        for sensor in sensors:
            sensor.start_measuring()
        time.sleep(10)
        for sensor in sensors:
            sensor.stop_measuring()
    except KeyboardInterrupt:
        for sensor in sensors:
            sensor.stop_measuring()
